package com.jpop.user.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.jpop.user.exceptions.UserNotFoundException;
import com.jpop.user.model.User;
import com.jpop.user.repository.UserRepository;
import com.jpop.user.responsemodel.UserResponse;

class UserServiceImplTest {
	@InjectMocks
	UserServiceImpl userService;

	@Mock
	UserRepository userRepo;

	@BeforeEach
	void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testUserServiceFindAllBooks() {
		List<User> allUsers = new ArrayList<>();
		long id = 1;
		User user = new User(id, "ThreeStates", "shakespere");
		allUsers.add(user);
		when(userRepo.findAll()).thenReturn(allUsers);
		assertEquals(userService.findAllUsers(), allUsers);

	}

	@Test
	void testUserServiceFindbyId() throws UserNotFoundException {

		long id = 1;
		User user = new User(id, "ThreeStates", "shakespere");
		Optional<User> result = Optional.ofNullable(user);

		when(userRepo.findById(id)).thenReturn(result);
		assertEquals(1, userService.findUserById(id).getUserId());

	}

	@Test
	void testUserServicebyIdwithNegativeCase() throws UserNotFoundException {
		long id = 1;
		User user = new User(id, "ThreeStates", "shakespere");
		Optional<User> result = Optional.ofNullable(user);

		when(userRepo.findById(id)).thenReturn(result);
		assertNotEquals(2, userService.findUserById(id).getUserId());

	}

	@Test
	void testCreateUser() {

		long id = 1;
		User user = new User(id, "ThreeStates", "shakespere");
		UserResponse userResponse = new UserResponse(id, "ThreeStates", "shakespere");
		when(userRepo.save(user)).thenReturn(user);
		assertEquals(userResponse.getUserId(), userService.createNewUser(user).getUserId());

	}

	@Test
	void testCreateBookWitNegativeCase() {

		long id = 1;
		User user = new User(id, "ThreeStates", "shakespere");
		UserResponse fakeUser = new UserResponse((long) 2, "threeSTates", "ronaldo");
		when(userRepo.save(user)).thenReturn(user);
		assertNotEquals(fakeUser.getUserId(), userService.createNewUser(user).getUserId());

	}

	@Test
	void testDelete() throws UserNotFoundException {
		doNothing().when(userRepo).deleteById((long) 1);
		assertEquals((long) 1, userService.deleteUser((long) 1));

	}

	@Test
	void testDeleteWithNegativeCase() throws UserNotFoundException {
		doNothing().when(userRepo).deleteById((long) 1);
		assertThrows(UserNotFoundException.class, () -> userService.deleteUser((long) 1));

	}

}
