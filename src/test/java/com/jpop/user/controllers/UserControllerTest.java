package com.jpop.user.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.jpop.user.exceptions.UserNotFoundException;
import com.jpop.user.model.User;
import com.jpop.user.responsemodel.UserResponse;
import com.jpop.user.service.UserService;



class UserControllerTest {

	@InjectMocks
	UserController userController;

	@Mock
	UserService userService;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testGetAllUsers() throws UserNotFoundException {
		List<UserResponse> usersResponse = new ArrayList<>();
		usersResponse.add(new UserResponse((long) 1, "ChristoperNolen", "Nolen123"));

		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		when(userService.getAllUserResponses()).thenReturn(usersResponse);
		ResponseEntity<List<UserResponse>> responseEntity = userController.getAllUsers();
		assertEquals("200 OK", responseEntity.getStatusCode().toString());

	}

	@Test
	void testUserbyId() throws UserNotFoundException {
		UserResponse user = new UserResponse((long) 1, "ChristoperNolen", "Nolen123");
		when(userService.getUserResponse((long) 1)).thenReturn(user);
		ResponseEntity<UserResponse> responseEntity = userController.getUserbyId((long) 1);
		assertEquals("200 OK", responseEntity.getStatusCode().toString());
		assertEquals(1, responseEntity.getBody().getUserId());

	}

	@Test
	void testUserbyIdwithnegativecase() throws UserNotFoundException {
		UserResponse user = new UserResponse((long) 1, "ChristoperNolen", "Dark123");
		when(userService.getUserResponse((long) 1)).thenReturn(user);
		ResponseEntity<UserResponse> responseEntity = userController.getUserbyId((long) 1);
		assertEquals("200 OK", responseEntity.getStatusCode().toString());
		assertNotEquals(2, responseEntity.getBody().getUserId());

	}

	@Test
	void testUserbyIdwithException() throws UserNotFoundException {
		when(userService.getUserResponse((long) 1)).thenThrow(UserNotFoundException.class);

		assertThrows(UserNotFoundException.class, () -> userController.getUserbyId((long) 1));

	}

	@Test
	void testUpdateUser() throws UserNotFoundException {
		User user = new User((long) 1, "ChristoperNolen", "DarkNight");
		UserResponse updatedUser = new UserResponse((long) 1, "ChristoperNolen", "Inception");
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		when(userService.getUserResponse((long) 1)).thenReturn(updatedUser);

		ResponseEntity<UserResponse> responseEntity = userController.updateUser((long) 1, user);
		assertEquals("200 OK", responseEntity.getStatusCode().toString());
		assertEquals(1, responseEntity.getBody().getUserId());
	}

	@Test
	void testUpdateWithWrongCase() throws UserNotFoundException {
		User user = new User((long) 1, "ChristoperNolen", "DarkNight");
		UserResponse updatedBook = new UserResponse((long) 1, "ChristoperNolen", "Inception");
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		when(userService.getUserResponse((long) 1)).thenReturn(updatedBook);

		ResponseEntity<UserResponse> responseEntity = userController.updateUser((long) 1, user);
		assertEquals("200 OK", responseEntity.getStatusCode().toString());
		assertNotEquals(2, responseEntity.getBody().getUserId());
	}

	@Test
	void testUpdateWithException() throws UserNotFoundException {
		when(userService.getUserResponse((long) 1)).thenThrow(UserNotFoundException.class);

		assertThrows(UserNotFoundException.class, () -> userController.updateUser((long) 1, null));

	}

	@Test
	void testAddUser() {
		User user = new User((long) 1, "ChristoperNolen", "DarkNight");
		UserResponse userResponse = new UserResponse((long) 1, "ChristoperNolen", "DarkNight");
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		when(userService.createNewUser(user)).thenReturn(userResponse);
		ResponseEntity<UserResponse> responseEntity = userController.createUser(user);
		assertEquals("201 CREATED", responseEntity.getStatusCode().toString());
		assertEquals(1, responseEntity.getBody().getUserId());

	}

	@Test
	void testAddBookWithNegativeCases() {
	User user = new User((long) 1, "ChristoperNolen", "DarkNight");
	UserResponse userResponse = new UserResponse((long) 1, "ChristoperNolen", "DarkNight");
	MockHttpServletRequest request = new MockHttpServletRequest();
	RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
	when(userService.createNewUser(user)).thenReturn(userResponse);
	ResponseEntity<UserResponse> responseEntity = userController.createUser(user);
	assertEquals("201 CREATED", responseEntity.getStatusCode().toString());
	assertNotEquals(2, responseEntity.getBody().getUserId());

	}

}
