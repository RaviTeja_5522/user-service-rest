package com.jpop.user.controllers;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RestController;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@RestController
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.jpop.user.repository")
public class PersistenceContext {
 
    private static final Logger LOG = LogManager.getLogger(PersistenceContext.class);
 
    @Value("${spring.datasource.url}")
    private String url;
 
    @Value("${spring.datasource.username}")
    private String userName;
 
    @Value("${spring.datasource.password}")
    private String password;
 
    @Bean(destroyMethod = "close")
    DataSource dataSource() {
        HikariConfig dataSourceConfig = new HikariConfig();
        LOG.info("getting url: {}", url);
        dataSourceConfig.setJdbcUrl(url);
        LOG.info("getting username: {}", userName);
        dataSourceConfig.setUsername(userName);
        LOG.info("getting password: {}", password);
        dataSourceConfig.setPassword(password);
        return new HikariDataSource(dataSourceConfig);
    }
 
    @Bean
    JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }
}