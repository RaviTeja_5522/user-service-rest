package com.jpop.user.controllers;

import java.net.URI;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.jpop.user.model.User;
import com.jpop.user.responsemodel.UserResponse;
import com.jpop.user.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController

public class UserController {
	private static final Logger logger = LogManager.getLogger(UserController.class);

	@Autowired
	UserService userService;

	@GetMapping("/users")
	public ResponseEntity<List<UserResponse>> getAllUsers() {
		List<UserResponse> userResponsesList = null;
		try {
			userResponsesList = userService.getAllUserResponses();
		} catch (NullPointerException e) {
			logger.error("No users are found", e);
			throw new NullPointerException("There are No users to show");
		}
		return ResponseEntity.ok().body(userResponsesList);

	}

	@ApiOperation(value = "View a single user", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved user"),
			@ApiResponse(code = 404, message = "User not found with id") })
	@GetMapping("/users/{user_id}")
	public ResponseEntity<UserResponse> getUserbyId(
			@ApiParam(value = "User ID from which the user will be retrieved", required = true) @PathVariable Long user_id) {
		UserResponse userResponse = null;
		try {
			userResponse = userService.getUserResponse(user_id);
		} catch (NullPointerException e) {
			logger.error("There is no user for id:{0}",user_id,e);
			throw new NullPointerException("There is no User for given id:" + user_id);

		}
		return ResponseEntity.ok().body(userResponse);

	}

	@ApiOperation(value = "Add New User", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully Created user"),

	})
	@PostMapping("/users")
	public ResponseEntity<UserResponse> createUser(
			@ApiParam(value = "User required details to create user", required = true) @RequestBody User user) {

		UserResponse userResponse = userService.createNewUser(user);
		final URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/users").build().toUri();
		return ResponseEntity.created(uri).body(userResponse);

	}

	@ApiOperation(value = "Update the user", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully Updated user"),
			@ApiResponse(code = 404, message = "User not found with id") })

	@PutMapping("/users/{user_id}")
	public ResponseEntity<UserResponse> updateUser(
			@ApiParam(value = "User ID from which the user will be Updated", required = true) @PathVariable Long user_id,
			@ApiParam(value = "Body for updating", required = true) @RequestBody User user) {
		UserResponse userResponse = null;
		try {
			userResponse = userService.getUserResponse(user_id);
		} catch (NullPointerException e) {
			logger.error("There is no user for id:{0}",user_id,e);
			throw new NullPointerException("There are No users to Update for user_id:" + user_id);

		}
		userService.createNewUser(user);
		return ResponseEntity.ok().body(userResponse);
	}

	@ApiOperation(value = "Delete the user", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully Deletd the user"),
			@ApiResponse(code = 404, message = "User not found with id") })

	@DeleteMapping("/users/{user_id}")
	public ResponseEntity deleteUser(
			@ApiParam(value = "User ID from which the user will be Deleted", required = true) @PathVariable Long user_id) {

		try {
			userService.deleteUser(user_id);
		} catch (NullPointerException e) {
			logger.error("There is no user for id:{0}",user_id,e);
			throw new NullPointerException("There is no User to delete for given id:" + user_id);
		}
		return ResponseEntity.noContent().build();
	}

}
