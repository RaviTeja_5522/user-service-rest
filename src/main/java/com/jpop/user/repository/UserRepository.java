package com.jpop.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jpop.user.model.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
