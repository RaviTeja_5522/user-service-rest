package com.jpop.user.exceptions;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MissingHeaderInfo extends RuntimeException
{
    private static final long serialVersionUID = 1L;
 
    public MissingHeaderInfo(String message) {
        super(message);
    }
}
