package com.jpop.user.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpop.user.model.User;
import com.jpop.user.repository.UserRepository;
import com.jpop.user.responsemodel.UserResponse;
@Service
public class UserServiceImpl implements UserService {

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	@Autowired
	UserRepository userRepo;

	public List<User> findAllUsers() {
		return userRepo.findAll();
	}

	public User findUserById(Long id) throws NullPointerException {
		return userRepo.findById(id).orElseThrow(() -> new NullPointerException("There is no user"));
	}

	public UserResponse createNewUser(User user) {
		user = userRepo.save(user);

		return new UserResponse(user.getUserId(), user.getUserName(), user.getPassword());
	}

	public User deleteUser(Long id) throws NullPointerException {
		User user = findUserById(id);
		if (user != null) {
			userRepo.deleteById(id);
		} else {
			logger.error("There is no user for id:{}",id);
			throw new NullPointerException("There is no such User");
		}

		return user;

	}

	public List<UserResponse> getAllUserResponses() throws NullPointerException {
		List<User> allUsers = findAllUsers();
		List<UserResponse> allUsersResponse = new ArrayList<>();
		if (allUsers.isEmpty()) {
			logger.error("There are No users in database");
			throw new NullPointerException("No users are available");
		} else {

			for (User user : allUsers) {
				UserResponse userResponse = new UserResponse(user.getUserId(), user.getUserName(), user.getPassword());
				allUsersResponse.add(userResponse);
			}
		}
		return allUsersResponse;

	}

	public UserResponse getUserResponse(Long id) throws NullPointerException {
		User user = findUserById(id);
		return new UserResponse(user.getUserId(), user.getUserName(), user.getPassword());
	}

}
