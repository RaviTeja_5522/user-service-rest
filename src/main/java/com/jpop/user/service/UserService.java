package com.jpop.user.service;

import java.util.List;

import com.jpop.user.model.User;
import com.jpop.user.responsemodel.UserResponse;

public interface UserService {

	public List<User> findAllUsers();

	public User findUserById(Long id) throws NullPointerException;

	public UserResponse createNewUser(User user);

	public User deleteUser(Long id) throws NullPointerException;

	public List<UserResponse> getAllUserResponses() throws NullPointerException;

	public UserResponse getUserResponse(Long id) throws NullPointerException;

}
