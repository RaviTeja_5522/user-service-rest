package com.jpop.user.responsemodel;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString
public @Data class UserResponse {
	private Long userId;
	private String userName;
	private String password;

	public UserResponse(Long userId, String userName, String password) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.password = password;
	}

}
